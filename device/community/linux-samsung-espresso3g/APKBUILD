# Maintainer: Mighty <mightymb17@gmail.com>
# Co-Maintainer: Antoni Aloy <aaloytorrens@gmail.com>
pkgname=linux-samsung-espresso3g
pkgver=5.15.2
pkgrel=3
pkgdesc="Samsung Galaxy Tab 2 (7.0 inch) mainline kernel"
arch="armv7"
_carch="arm"
_flavor="samsung-espresso3g"
url="https://kernel.org"
license="GPL2"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-anbox
	pmb:kconfigcheck-containers
	pmb:kconfigcheck-nftables
	pmb:kconfigcheck-zram"
makedepends="openssl-dev yaml-dev mpc1-dev mpfr-dev xz findutils bison flex perl sed bash gmp-dev bc linux-headers elfutils-dev"
_commit="6ba3430a6fad45bf35f2634809e4f3a12f85cb89"
_config="config-$_flavor.$arch"
source="
	$pkgname-$_commit.tar.gz::https://github.com/tmlind/linux_openpvrsgx/archive/$_commit.tar.gz
	00-add-espresso-dts.patch
	02-CVE-2021-39685-USB-gadget-detect-too-big-endpoint-0-requests.patch
	03-CVE-2021-39685-USB-gadget-zero-allocate-endpoint-0-buffers.patch
	04-Add-TWL6030-power-driver-with-minimal-support-for-power-off.patch
	05-Add-TWL6030-power-button-support-to-twl-pwrbutton.patch
	$_config
"
builddir="$srcdir/linux_openpvrsgx-$_commit"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS"
}

package() {
	install -Dm644 "$builddir/arch/$_carch/boot/"*zImage \
		"$pkgdir/boot/vmlinuz"

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"
}

sha512sums="
c1f1372afc303cabb55210c7c46a694eff3bc0d7871efee7a690488b782b0c2cb48dbccc43b454a7cf96cfea796397f621d338e791172a8d1f62b26b2e1d54cb  linux-samsung-espresso3g-6ba3430a6fad45bf35f2634809e4f3a12f85cb89.tar.gz
e3e6ecc15b477531cb06200dbe17d1a3f8ddf6c403b8a8ec78ed73b5b1497fbf3492f074ac9e44bd252d8e8fb311ee910113ac1890de93adee445de3a644ea33  00-add-espresso-dts.patch
c83480686caa35c51bce654104082e51d2569850bbbdcdb8479fb756ffb7907aefce685b2cfa748bbed0da7b585be83a08d194d0ff315a070a5b5a07c8dbc1d2  02-CVE-2021-39685-USB-gadget-detect-too-big-endpoint-0-requests.patch
7b76e82bca21c9746bb37df2e840b43a0628a8a00b45ee43dd38ce742d7b99e30faf4bd11c99f1a20299b486885cbb9f62502400544a6a7e319292b97331581d  03-CVE-2021-39685-USB-gadget-zero-allocate-endpoint-0-buffers.patch
a483b1a322f3fe47c7bb6514f34cca69ad1b806487596ffab69078acb6e83e99ed39ffb4d5b4ecd17035d118a75ce5bac6761b9e42d01608f1e6a53d59b27806  04-Add-TWL6030-power-driver-with-minimal-support-for-power-off.patch
b3d6114c5c60fc2820856c89ea6f09c369d857ae79a79f0eeb0f83f5401dc2253e11f7dac6869eb1095d3e0b3a68126246762e2f406ffae5b5ef0a60d5563bac  05-Add-TWL6030-power-button-support-to-twl-pwrbutton.patch
f5d1b1ae3a776b4f68914b0ba00bbce8e00122028eb1141ea57dbe8d6d652d9a407f13df3d36e553e21c4b2d576bb0f8978b396a7af4511fad9a321e0ffc38ed  config-samsung-espresso3g.armv7
"
